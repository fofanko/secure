from igram.view import mainView, message, deleteItem
from auth.view import login,logout, changePass, registr
from flask import send_file
from flask import request, session

def hello_world():
    return 'Hello World!'

def getImage():
    # print("Shadow request")
    print(request.headers)
    return send_file("/home/vova/etu/secure/snd.jpg", mimetype='image/jpg')

def route(app):
    app.add_url_rule('/', 'hello', hello_world)
    app.add_url_rule('/igram', 'main', mainView, methods=['GET', 'POST'])
    app.add_url_rule('/img.jpg', 'img', getImage, methods=['GET'])

    app.add_url_rule('/igram/login', 'login', login, methods=['POST'])
    app.add_url_rule('/igram/logout', 'logout', logout, methods=['GET'])
    app.add_url_rule('/igram/changePass', 'changePass', changePass, methods=['GET', 'POST'])

    app.add_url_rule('/igram/registr', 'registr', registr, methods=['GET', 'POST'])

    app.add_url_rule('/igram/msg', 'message', message, methods=['GET', 'POST'])

    app.add_url_rule('/del/<int:item_id>', 'del', deleteItem, methods=['GET'])

