from flask import render_template
from app import app
from flask import request

from flask import redirect, url_for, session
from flask import abort

from db import get_db

def login():
    if request.method == "POST":
        if not("name" in request.form and "passwd" in request.form):
            abort(404)
        name, passwd = request.form['name'], request.form['passwd']
        db = get_db()
        with db.cursor() as cur:
            cur.execute("SELECT * FROM users WHERE username=%s AND passwd=%s", [name, passwd])
            is_exitst = cur.fetchall()
        db.close()
        if is_exitst:
            session["user"] = {'name': name}
        return redirect(url_for('main'))
    abort(404)


def logout():
    if "user" in session:
        session.pop("user")
    return redirect(url_for('main'))    

def changePass():
    if request.method == "GET":
        if "user" not in session:
            return redirect(url_for('main'))
        return render_template("auth/changePass.html")
    elif request.method == "POST":
        if "user" not in session:
            return redirect(url_for('main'))
        if not("new_pass" in request.form and "new_pass2" in request.form):
            abort(404)
        pass1, pass2 = request.form['new_pass'], request.form['new_pass2']
        if pass1 != pass2:
            abort(404)
        db = get_db()
        with db.cursor() as cur:
            cur.execute("UPDATE users SET passwd=%s WHERE username=%s;", [pass1, session['user']['name']])
        db.commit()
        db.close()
        return redirect(url_for('main'))

def registr():
    if request.method == "GET":
        return render_template("auth/reg.html")
    elif request.method == "POST":
        if not("name" in request.form and "password" in request.form):
            abort(404)
        name, passwd = request.form['name'], request.form['password']
        db = get_db()
        with db.cursor() as cur:
            cur.execute("INSERT INTO users (username, passwd) VALUES (%s, %s); ", [name, passwd])
        db.commit()
        session["user"] = {'name': name}
        db.close()
        return redirect(url_for('main'))