from flask import render_template
from app import app
from flask import request

from flask import redirect, url_for, session
from flask import abort
from db import get_db

def mainView():
    db = get_db()
    if request.method == "POST":
        name = request.form['name']
        img_link = request.form['img_link']
        print(img_link)
        with db.cursor() as cur:    
            cur.execute("INSERT INTO item (name, src) VALUES (%(name)s, %(src)s); ", {"name": name, "src": img_link})
        db.commit()
        redirect(url_for('main'))
    with db.cursor() as cur:
        cur.execute("SELECT name, src, id FROM item ORDER BY id DESC;")
        items = cur.fetchall() 
        print(items) 
    db.close()
    return render_template("igram/index.html", items=items)

def deleteItem(item_id):
    if "user" in session:
        redirect(url_for('main'))
    name = session['user']['name']
    db = get_db()
    with db.cursor() as cur:    
        cur.execute("SELECT * FROM item WHERE name=%s AND id=%s;", [name, item_id])
        is_exist = cur.fetchall()
        is_exist = True if name == "admin" else is_exist
        if is_exist:
            cur.execute("DELETE FROM item WHERE id=%s;", [item_id])
            db.commit()
    db.close()
    return redirect(url_for('main'))

def message():
    if "user" in session:
        redirect(url_for('main'))
    db = get_db()
    if request.method == "POST":
        if not("consumer" in request.form and "text" in request.form):
            redirect(url_for('message'))
        consumer, text = request.form['consumer'], request.form['text']
        with db.cursor() as cur:
            cur.execute("SELECT id FROM users WHERE username=%s", [ session['user']['name'] ])
            id_produser = cur.fetchone()
            cur.execute("SELECT id FROM users WHERE username=%s", [ consumer ])
            id_consumer = cur.fetchone()
            if not( id_consumer and id_produser):
                abort(404)
            cur.execute("INSERT INTO message (consumer, produser, text) VALUES \
                (%s, %s, %s); ", [id_consumer, id_produser, text])
        db.commit()
        return redirect(url_for('message'))
    elif request.method == "GET":
        with db.cursor() as cur:
            name = session['user']['name']
            cur.execute("SELECT (SELECT username FROM users WHERE id=produser), text FROM message JOIN users ON message.consumer=users.id WHERE users.username=%s;", [name])
            msgs = cur.fetchall()
        db.close()
        return render_template("igram/msgs.html", msgs=msgs)
    
        
